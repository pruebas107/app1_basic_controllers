//
//  ViewController.swift
//  App1_Basic_Controllers
//
//  Created by MTWDM_2022 on 30/09/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBOutlet var lblnumero: UILabel!
    var numero: Int = 0

    @IBAction func onContar(_ sender: Any) {
        numero = numero + 1
        lblnumero.text = String(numero)
    }
    
    @IBAction func onLimpiar(_ sender: Any) {
        lblnumero.text = "0"
        numero = 0
    }

   
}

