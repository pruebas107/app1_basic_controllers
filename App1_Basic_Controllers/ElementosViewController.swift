//
//  ElementosViewController.swift
//  App1_Basic_Controllers
//
//  Created by MTWDM_2022 on 30/09/22.
//

import UIKit

class ElementosViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        cajatexto.becomeFirstResponder()
        
        createToolbar()
        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBOutlet var cajatexto: UITextField!
    
    func createToolbar() {
        let toolbar = UIToolbar()
        
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Aceptar", style: .done, target: self, action: #selector(dismissKeyboard))
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem:.flexibleSpace, target: self, action: nil)
        
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        
        cajatexto.inputAccessoryView = toolbar
           
        
    }
    
    @objc func dismissKeyboard(){
        cajatexto.resignFirstResponder()
        
    }
    
    

}
