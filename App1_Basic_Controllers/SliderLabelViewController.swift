//
//  SliderLabelViewController.swift
//  App1_Basic_Controllers
//
//  Created by MTWDM_2022 on 30/09/22.
//

import UIKit

class SliderLabelViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblGrupo.textColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: CGFloat(sliderAlfa.value))
        
    }
    

    @IBOutlet var lblGrupo: UILabel!
    
    @IBOutlet var sliderAlfa: UISlider!
    
    @IBAction func OnChangeValue(_ sender: UISlider) {
        
    let slider  = sender as! UISlider
        
        lblGrupo.textColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: CGFloat(slider.value))
    }
    
}
